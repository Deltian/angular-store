﻿alertify.dialog("loginModal", function () {
    return {
        main: function (content) {
            this.setContent(content);
        },
        setup: function () {
            return {
                options: {
                    title: "Zaloguj się",
                    closable: true,
                    basic: true,
                    maximizable: false,
                    resizable: false,
                    padding: true,
                    modal: true,
                    movable: false
                }
            }
        }
    }
});

function openLogonModal() {
    var loginModalDiv = $("#loginModal").css("display", "block")[0];

    alertify.loginModal(loginModalDiv);
}